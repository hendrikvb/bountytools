#!/bin/bash
mkdir gobuster
for i in $( cat ${1} ); do
 url=${i#*//}
 gobuster dir --wordlist /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -s 200,301 -t 10 -f --wildcard -u ${i} -e -x html,php,txt,xml,js,json | tee gobuster/${url}.gobuster
done
