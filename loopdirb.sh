#!/bin/bash
mkdir dirb
for i in $( cat $1 ); do
  url=${i#*//}
  cp dicc.txt customdicc.txt
  cat robot/$url.wordlist >> customdicc.txt 
  echo "Processing $url"
  dirb $i customdicc.txt -N 403,429,400,301 -f -X .html,.txt,.js,.php,.asp,.json,.jsp,.xml,.pl,.jsonp,.aspx,.asmx,.wadl -R 3 -o dirb/$url.dirb -a "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; WOW64; Trident/4.0; SLCC1) Intigriti"
done
find dirb -size  0 -print0 |xargs -0 rm --
