#!/bin/bash
url=${i#*//}
mkdir gobuster
gobuster -l -w /usr/share/wordlists/dirb/big.txt -fw -k -t 20 -u $1 -e -f -x html,txt,js,php,asp,json,jsp,xml | tee gobuster/${url}_gobust.txt
