#!/bin/bash
mkdir dnsenum
for i in $( cat ${1} ); do
 url=${i#*//}
 dnsenum --file top1mil.txt $i --threads 20 --noreverse --dnsserver 8.8.8.8 --subfile dnsenum/${url}.dnsenum
done
