#!/bin/bash
mkdir paramspider 
echo > paramspider/result_filtered.txt
for i in $( cat $1 ); do
  url=${i#*//}
  echo "Processing ${url}" 
  python3 /opt/ParamSpider/paramspider.py --domain ${i} --exclude svg,jpg,css,js --level high --output paramspider/result.txt 
  echo $url > paramspider/result_filtered.txt
  /root/go/bin/gf xss output/paramspider/result.txt >> paramspider/result_xss.txt
  /root/go/bin/gf redir output/paramspider/result.txt >> paramspider/result_redir.txt
done
