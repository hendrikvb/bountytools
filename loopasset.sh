#!/bin/bash
mkdir asset
for i in $( cat ${1} ); do
 url=${i#*//}
 assetfinder --subs-only $i | httprobe  | tee asset/$url.asset
done
