#!/bin/bash
mkdir xsstrike 
echo > xsstrike/results.txt
for i in $( cat $1 ); do
  url=${i#*//}
  echo "Processing $i"
  echo "Processing $i" >> xsstrike/results.txt
  python3 /opt/XSStrike/xsstrike.py -u "$i" | tee -a xsstrike/results.txt 
done
