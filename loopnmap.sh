#!/bin/bash
mkdir nmap 
for i in $( cat $1 ); do
  url=${i#*//}
  echo "Processing $url"
  nmap -T2 $url -F -oN nmap/$url.txt
done
find nmap -size  0 -print0 |xargs -0 rm --
