# Bountytools

Set of wrapper shell scripts used to simplify enumeration of domains for bug bounties.
Output is typically stored in subdirectory with specific extension. Scripts take file as argument. Read code before use!
- dnsenum.sh: dnsenum wrapper - single domain usage - Pass domain as parameter
- gobuster.sh: gobuster wrapper - single URL usage - Pass URL as parameter
- loopamass.sh: amass loop wrapper - multiple domain usage - Pass domain list file as parameter
- loopasset.sh: assetfinder loop wrapper - multiple domain usage - Pass domain list file as parameter
- dirsearch.sh: dirsearch loop wrapper - multiple URL usage - Pass URL list file as parameter
- loopdnsenum.sh: loop version of dnsenum.sh - Pass domain list file as parameter
- loopeye.sh: cutycapt loop wrapper - multiple domain usage - Pass domain list file as parameter
- loopgob.sh: loop version of gobuster.sh - multiple URL usage - Pass URL list file as parameter
- loopnikto.sh: nikto loop wrapper - multiple URL usage - Pass URL list file as parameter
- looprobot.sh: wget robots.txt loop wrapper - multiple URL usage - Pass URL list file as parameter


Requirements
------------
- top1mil.txt (current folder)
- dirbuster/directory-list-2.3-medium.txt (default Kali location)


Disclaimer
----------
Use script as-is!

These scripts are wrapper scripts around existing security tools and OBVIOUSLY can do as much harm as the original tools.
These scripts will try to create subfolders to store enumeration results. Folders are created in the current working path.