#!/bin/bash
mkdir dirsearch
for i in $( cat $1 ); do
  url=${i#*//}
  echo "Processing $url"
  /opt/dirsearch/dirsearch.py -x 301,400,404,403,521 -u $i -e html,txt,js,php,asp,json,jsp,xml -r --plain-text-report=dirsearch/$url.dirsearch
done
