#!/bin/bash
mkdir corsy 
pwd=$PWD
for i in $( cat $1 ); do
  cd /opt/corsy
  url=${i#*//}
  echo "Processing $url"
  if [[ $i != *"http"* ]]
  then
	  i="https://$i"
  fi
  python3 corsy.py -u $i > $pwd/corsy/$url.cors
  cd $PWD
done
