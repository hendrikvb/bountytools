#!/bin/bash
mkdir webshots
for i in $( cat ${1} ); do
 url=${i#*//}
 echo Grabbing $url
 cutycapt --url=http://${i}.${1} --out=webshots/${url}_http.png
 cutycapt --url=https://${i}.${1} --out=webshots/${url}_https.png 
done
