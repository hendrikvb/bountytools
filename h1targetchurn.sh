#!/bin/bash

# Rough HackerOne JSON target parser and churner
# This script dumps the HackerOne JSON data from the arkadiyt git repo and extracts the target name.
# Top level domains will be stored in each program subfolder as top.txt, additional checks are done with httprobe and stored in domains.txt
# Subdomains with wildcards are stored in each program subfolder as sub.txt
# Each wildcard subdomain will be parsed and passed to assetfinder and amass. Additional checks are done with httprobe. Results are stored in each program subfolder. 
#
# All program domain URLs are stored in domains.txt
# All program subdomain URLs are stored in subdomains.txt
# All HTTP subdomain URLs are stored in subdomains_http.txt
# All HTTPS subdomain URLs are stored in subdomains_https.txt

# USE AT OWN RISK - WORK IN PROGRESS
# @hendrikvb 

# Last Fix: 1 July 2020
# Pass filter string as argument. 
# ./h1targetchurn.sh Zendesk
# will only process Zendesk targets

curl -s https://raw.githubusercontent.com/arkadiyt/bounty-targets-data/master/data/hackerone_data.json --output hackerone_data.json 
cat hackerone_data.json | jq -r '.[].name' | sort -u > hackerone_programs.txt

# Limit scope to first parameter passed
if [ ! -z "$1" ]
  then
    cat hackerone_data.json | jq -r '.[].name' | sort -u | grep $1 > hackerone_programs.txt 
fi

for i in $( cat hackerone_programs.txt ); do
 clean=$( echo $i | tr -d "[\., \+\(\)\[\]\'&]" )
 mkdir -p "hackerone/$clean"
 echo "Dumping $i into hackerone/$clean/top.txt"
 cat hackerone_data.json | jq -r '.[] | select(.name=="'"$i"'") | .targets.in_scope[] | select(.asset_type|contains("URL")) | .asset_identifier' | awk -F'/' '{print $1}' | grep -v "*" | sort -u > "hackerone/$clean/top.txt"
 cat hackerone_data.json | jq -r '.[] | select(.name=="'"$i"'") | .targets.in_scope[] | select(.asset_type|contains("URL")) | .asset_identifier' | awk -F'/' '{print $1}' | grep "*" | sort -u > "hackerone/$clean/sub.txt"
 for x in $( cat hackerone/$clean/sub.txt ); do
    cleansubdomain=$(echo $x | sed 's/\*\.//g')
    url=${x#*//}
    amass enum -passive -d $cleansubdomain | httprobe -p xlarge | tee -a hackerone/$clean/${cleansubdomain}_amass.txt
    assetfinder --subs-only $cleansubdomain | httprobe -p xlarge | tee -a hackerone/$clean/${cleansubdomain}_asset.txt
  done
  cat hackerone/$clean/top.txt | httprobe -p xlarge > hackerone/$clean/domains.txt
  cat hackerone/$clean/*_asset.txt hackerone/$clean/*_amass.txt | sort | uniq > hackerone/$clean/subdomains.txt
  cat hackerone/$clean/subdomains.txt | grep -v 'https' > hackerone/$clean/subdomains_http.txt
  cat hackerone/$clean/subdomains.txt | grep 'https' > hackerone/$clean/subdomains_http.txt
done
