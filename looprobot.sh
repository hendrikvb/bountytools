#!/bin/bash
mkdir robot
for i in $( cat $1 ); do
  url=${i#*//}
  echo "Processing $url"
  wget $i/robots.txt --tries=1 -T 2 -O robot/$url.robot 
  ./doroboty.pl $i > robot/$url.wordlist
  wget $i/sitemap.xml --tries=1 -T 2 -O robot/$url.sitemap
done
find robot -size  0 -print0 |xargs -0 rm --
