#!/bin/bash
mkdir dirsearch
url=${1#*//}
echo "Processing $url"
/opt/dirsearch/dirsearch.py -x 403,429 -u $1 -e html,txt,js,php,asp,json,jsp,xml,pl,jsonp,aspx,asmx -R 3 -F --ua="Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; WOW64; Trident/4.0; SLCC1) bugcrowd" --plain-text-report=dirsearch/$url.dirsearch
