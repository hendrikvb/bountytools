#!/bin/bash
mkdir hak
mkdir hakraw
mkdir paths
for i in $( cat ${1} ); do
 url=${i#*//}
 ~/go/bin/hakrawler -domain $url -depth 3 -scope fuzzy -outdir hakraw -plain | tee -a hak/$url.txt
 perl paths.pl hak/$url.txt | sort | uniq > paths/paths_${url}.txt
done
cat paths/* | sort | uniq >> paths.txt
echo "hakrawler discovered paths are in ./hak"
echo "raw hakrawler data is in ./hakraw"
echo "discovered paths per domain is in ./paths"
echo "consolidated paths are in paths.txt"
