#!/bin/bash
mkdir httrack 
for i in $( cat $1 ); do
  url=${i#*//}
  mkdir httrack/$url
  echo "Processing $url"
  httrack $i -mime:image/* -O httrack/$url
done
find dirsearch -size  0 -print0 |xargs -0 rm --
