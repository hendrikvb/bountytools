#!/bin/bash
mkdir amass
for i in $( cat ${1} ); do
 url=${i#*//}
 amass enum -passive -d $i | httprobe | tee amass/$url.amass
done
