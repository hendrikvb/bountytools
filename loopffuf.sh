#!/bin/bash
# Typically pass subdomains.txt as first params
# This will only do https sites (adjust where needed)
# This also parses the output from loophakrawler!!
perl paths.pl hak/* | sort | uniq > paths.txt
mkdir ffuf 
for i in $( cat $1 | httprobe | grep https ); do
  url=${i#*//}
  cp dicc.txt customdicc.txt
  cat robot/$url.wordlist >> customdicc.txt 
  for p in $( cat paths.txt ); do
    u="$i/$p"
    echo "Processing $i/$p"
    fuzzurl=${u}FUZZ
    /root/go/bin/ffuf -c -w customdicc.txt -u $fuzzurl -fc 301,403 | tee ffuf/${url}.txt
  done
done
find ffuf -size  0 -print0 |xargs -0 rm --
