#!/bin/bash
mkdir nikto
for i in $( cat ${1} ); do
 url=${i#*//}
 nikto -h $i | tee nikto/${url}.txt
done
